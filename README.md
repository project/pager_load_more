# Pager Load More 🚀🤘🎸🔥

For instructions on how to install this module, visit the [Drupal Wiki](https://www.drupal.org/docs/extending-drupal/installing-modules).

## Add Pager Load More to a View

1. Create or edit a view (/admin/structure/views)
2. Under `Pager` click the link next to `Use pager:` which may read "Full", "Mini", "Display all items", "Display a specified number of items", etc.
3. Choose `Pager Load More` and click `Apply`.
4. On the `Pager options` screen, change the settings as below and click `Apply`.
5. You must click `Save` at the bottom left of the UI to commit changes to the view.

## View-specific Pager Load More Settings

TBD

## Sitewide Pager Load More Settings

Visit `/admin/config/user-interface/pager-load-more` on your site
or click Admin > Config > User Interface > Pager Load More.

TBD

## Additional Help or Support

If you need help, jump into [#florida on Drupal Slack](https://www.drupal.org/slack).

If you have a suggestion or want to contribute code, please visit the [Pager Load More module issue queue on Drupal.org](https://www.drupal.org/project/issues/pager_load_more).
