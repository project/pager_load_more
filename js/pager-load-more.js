/**
 * @file
 * Functionality for the Pager Load More module.
 *
 * @see https://www.drupal.org/project/pager_load_more
 */

((Drupal, drupalSettings, once) => {
  const currentURL = `${window.location.protocol}//${window.location.host}${window.location.pathname}`;
  const pagerContainerSelector = drupalSettings.pager_load_more.pager_container_selector;
  const viewSelector = `:is(${drupalSettings.pager_load_more.view_selector}):not(.attachment *)`;
  Drupal.pagerLoadMore = {};

  /**
   * Checks various browser features to determine if pager load more should run.
   *
   * @return {boolean} Whether the current browser supports everything we do.
   */
  function browserSupportExists() {
    return (
      window.fetch &&
      Element.prototype.closest &&
      Element.prototype.after &&
      Element.prototype.append &&
      NodeList.prototype.forEach &&
      String.prototype.includes &&
      String.prototype.replaceAll
    );
  }

  /**
   * Gets the `page` hash value from the URL and sanitizes.
   *
   * @return {string} - Sanitized hash value obtained from the URL.
   */
  function getPageHashFromUrl() {
    const hash = decodeURI(window.location.hash ? window.location.hash.replace('#page', '') : 0);
    const regex = /[^,|\d]/g; // Matches everything except commas and numbers.
    return hash.replaceAll(regex, '');
  }

  /**
   * Retrieves and deconstructs the `page` hash from the URL, and returns the
   * page number for the passed in ViewID. This is used when loading previous pages.
   *
   * This is important when there are multiple views on a page. The page hash will
   * look something like `,2,,3`. If the pagerId is 4, we only care about that page
   * (which in this case is 3).
   *
   * @param {number} pagerId - ID of the pager.
   *
   * @return {number} current page for passed in View ID.
   */
  function getCurrentPageNumberFromURL(pagerId) {
    const paramValue = getPageHashFromUrl();
    const paramArray = paramValue.split(',');

    return parseInt(paramArray[pagerId] || 0);
  }

  /**
   * Looks at the View's DOM and passes the values need to query and append
   * the content based on the view type.
   *
   * @param  {HTMLElement} viewEl - HTML Element of the view
   *
   * @return {Object} - Object containing `type`, `contentContainerSelector`, and `newContentHtmlElementType`.
   */
  function getViewType(viewEl) {
    let contentContainerSelector = '';
    let type = '';
    let newContentHtmlElementType = '';
    const exists = (selector) => viewEl.querySelectorAll(selector).length > 0;

    switch (true) {
      case exists('.views-view-responsive-grid'):
        type = 'responsive_grid';
        contentContainerSelector = '.views-view-responsive-grid';
        newContentHtmlElementType = 'section';
        break;
      case exists('tbody'):
        type = 'table';
        contentContainerSelector = 'tbody';
        newContentHtmlElementType = 'tbody';
        break;
      case exists('.view-content > .item-list'):
        type = 'list';
        contentContainerSelector = '.view-content > .item-list > *';
        newContentHtmlElementType = 'div';
        break;
      default:
        type = 'default';
        contentContainerSelector = '.view-content';
        newContentHtmlElementType = 'section';
    }

    return {
      newContentHtmlElementType,
      contentContainerSelector,
      type,
    };
  }

  /**
   * We accommodate multiple views on a single page.
   *
   * This is done by querying the DOM for all views, then getting the index of
   * the current View from the nodeList.
   *
   * @param {HTMLElement} currentViewEl - the current Views element.
   *
   * @return {Number} The index of the current view within the NodeList when selecting for all Views.
   */
  function getViewId(currentViewEl) {
    const viewsList = Array.from(document.querySelectorAll(viewSelector));
    const id = viewsList.findIndex((el) => {
      return el === currentViewEl;
    });

    return id;
  }

  /**
   * Returns a NodeList of focusable elements that exist within the passed HTMLElement.
   *
   * @param {HTMLElement} parent HTML element
   * @return {NodeList} The focusable elements that we can find
   */
  function getAllFocusableElements(parent) {
    return parent.querySelectorAll(':is(button, [href], input, select, textarea, [tabindex], details, summary):not(:is([disabled], [tabindex="-1"]))');
  }

  /**
   * Generates and returns a new hash with updated values.
   *
   * @param {number} pageNumber - Page number.
   * @param {string} pagerId - ID of the pager.
   *
   * @return {string} The updated value for the `page` hash (without the 'page')
   */
  function generatePageHash(pageNumber, pagerId) {
    const currentHashValue = getPageHashFromUrl();
    const hashArray = currentHashValue.split(',');

    // The hash has a format of `2,,,4`, where 2 is the page number of page 0,
    // pages 1, and 2 have a are empty (which is the same as a page number of
    // zero) and page 3 has a page number of 4. There may also be more pages
    // after page 3 (if they do exist, they have page numbers of 0). So,
    // the pager ID might not exist in the old hash. If it does, we update it.
    // If it doesn't we have to loop through and add a comma for each pagerId
    // number that exists between the last pagerId and the one that we want to
    // update.
    for (let i = 0; i <= pagerId; i++) {
      if (i === pagerId) {
        hashArray[pagerId] = pageNumber;
      } else if (!hashArray[pagerId]) {
        hashArray.push('0');
      }
    }

    const newHashValue = hashArray.join(',');

    return newHashValue;
  }

  /**
   * Generates a value for the `page` PARAMETER that will be used in the querystring
   * of the URL to fetch.
   *
   * This is different than the `generatePageHash()` function because we only care
   * about the page number that is being passed in. All other pagerId's page
   * numbers will be removed.
   *
   * @param {number} pageNumber - the page number.
   * @param {number} pagerId - the ID of the pager.
   *
   * @return {string} - The finalized page parameter, which will look something like
   *  `,,,3` if the pageNumber is 3, and the pagerId is 4.
   */
  function generatePageParameter(pageNumber, pagerId) {
    const pageNumberArray = [];
    for (let i = 0; i <= pagerId; i++) {
      if (i !== pagerId) {
        pageNumberArray.push('');
      } else {
        pageNumberArray.push(pageNumber);
      }
    }

    const parameter = pageNumberArray.join(',');
    return parameter;
  }

  /**
   * Informs on whether a 'next' link appears within the passed in pager.
   *
   * @param {Element} pagerContainer - The container of the pager.
   * @return {boolean} - whether a 'next' link appears within this pager.
   */
  function hasNextLink(pagerContainer) {
    const nextLink = pagerContainer.querySelectorAll('[rel="next"]');

    return nextLink.length;
  }

  /**
   * Fetch data in the specified page.
   *
   * @param {string} pageParameterToFetch - Value of the next page's `page` parameter.
   * @param {string} viewId - ID of the view to fetch.
   *
   * @return { Promise } Promise object which returns an array of elements.
   * */
  function fetchData(pageParameterToFetch, viewId) {
    // Get page's querystring and update the 'page' parameter.
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.set('page', pageParameterToFetch); // Update page parameter.
    const fetchURL = `${currentURL}?${urlParams.toString()}`;

    return fetch(fetchURL)
      .then((response) => {
        return response.text();
      })
      .then((htmlText) => {
        const { viewIndex, contentContainerSelector, pagerId, morePagesExist, pagerContainer } = Drupal.pagerLoadMore[viewId];
        const parser = new DOMParser();
        const nextPageDOM = parser.parseFromString(htmlText, 'text/html');
        const nextPageView = nextPageDOM.querySelectorAll(viewSelector)[viewIndex];
        const nextPagePager = nextPageView.querySelector(pagerContainerSelector);
        const nextPageNodeList = nextPageView.querySelectorAll(`${contentContainerSelector} > *`);

        // Clear any errors.
        pagerContainer.querySelector('.pager-load-more__error-container').innerHTML = '';
        Drupal.pagerLoadMore[viewId].currentPage = getCurrentPageNumberFromURL(pagerId); // @todo this should be in update URL has function

        // Only update if previous value is true. This is because fetches of may
        // multiple pages return asynchronously, and and the last page (where more
        // pages do not exist) may be returned *before* the other pages (where more
        // pages do exist).
        if (morePagesExist) {
          Drupal.pagerLoadMore[viewId].morePagesExist = hasNextLink(nextPagePager);
        }

        return Array.from(nextPageNodeList);
      })
      .catch((error) => {
        const { pagerContainer } = Drupal.pagerLoadMore[viewId];
        const errorMessage = Drupal.t('There was a problem loading new data.');
        const errorEl = document.createElement('div');

        errorEl.classList.add('pager-load-more__error-item');
        errorEl.textContent = errorMessage;
        pagerContainer.querySelector('.pager-load-more').classList.remove('is-loading');
        Drupal.announce(errorMessage);
        pagerContainer.querySelector('.pager-load-more__error-container').append(errorEl);
        console.error({ error }); // eslint-disable-line no-console
      });
  }

  /**
   * Builds an element that will hold the newly fetched content. If not a table view,
   * it will apply aria attributes.
   *
   * @param {string} viewId - The ID of the View.
   *
   * @return {Element} - The element that will eventually contain the newly fetched data.
   */
  function buildNewContainer(viewId) {
    const { newContentHtmlElementType, currentPage } = Drupal.pagerLoadMore[viewId];
    const contentEl = document.createElement(newContentHtmlElementType);
    const newPageNumber = parseInt(currentPage) + 1;
    const contentId = `pager-load-more--container-id${viewId}-page${newPageNumber}`;
    contentEl.setAttribute('id', contentId);

    if (newContentHtmlElementType !== 'table') {
      contentEl.setAttribute('style', 'display: contents');
    }

    if (newContentHtmlElementType === 'section') {
      // Create a visually hidden <span> which is associated to contentEl via
      // aria-labelledby, which provides better accessibility and translatability.
      const labelEl = document.createElement('span');
      const labelId = `pager-load-more--label-id${viewId}-page${newPageNumber}`;
      labelEl.setAttribute('id', labelId);
      labelEl.classList.add('pager-load-more__section-label', 'visually-hidden');
      labelEl.textContent = Drupal.t('Page @pageNumber items', { '@pageNumber': newPageNumber });
      contentEl.prepend(labelEl);
      contentEl.setAttribute('aria-labelledby', labelId);
      contentEl.setAttribute('style', 'display: contents');
    }

    return contentEl;
  }

  /**
   * Append data to the DOM.
   *
   * @param {Array} data - Array of elements.
   * @param {string} viewId - the ID of the view to on which to append the data.
   * @param {boolean} setFocus - Should we set focus after loading the data?
   */
  function appendDataToView(data, viewId, setFocus = true) {
    const { pagerContainer, isTableView, viewEl, morePagesExist, contentContainerSelector } = Drupal.pagerLoadMore[viewId];
    // On error no data is returned, so we exit.
    if (!data) return;

    // If no more data is to be had, hide the button. This should not happen
    // because the load more button should be hidden if the normal pager does
    // not have a "next" button.
    if (!data.length) {
      const loadMoreButton = pagerContainer.querySelector('.pager-load-more__button');
      loadMoreButton.classList.add('visually-hidden', 'focusable');
      loadMoreButton.setAttribute('disabled', true);
      Drupal.announce(Drupal.t('There are no more items.'));
      return;
    }

    const contentEl = buildNewContainer(viewId);

    // The existing element that the new contentEl container will be placed in.
    let thisContentContainer;
    if (isTableView) {
      thisContentContainer = viewEl.querySelector('.views-table'); // This is different than the table view content selector.
    } else {
      thisContentContainer = viewEl.querySelector(contentContainerSelector);
    }

    data.forEach((currentValue) => {
      contentEl.append(currentValue);
    });

    if (!isTableView) {
      thisContentContainer.append(contentEl);
    } else {
      thisContentContainer.querySelector('tbody:last-of-type').after(contentEl);
    }

    // If the fetched page does not have a "next" link in the default pager, we
    // assume that there are no more pages, and we hide the load more button.
    if (!morePagesExist) {
      pagerContainer.querySelector('.pager-load-more__button').classList.add('pager-load-more__button--hidden');
    }

    // If focusable element exists, then focus the first one.
    if (setFocus) getAllFocusableElements(contentEl)[0]?.focus();

    pagerContainer.querySelector('.pager-load-more').classList.remove('is-loading');

    Drupal.announce(Drupal.t('@count additional items have been loaded', { '@count': data.length }));
    Drupal.attachBehaviors(contentEl);
  }

  /**
   * Load all previous pages including the current page (as indicated by the hash) into the DOM.
   */
  function loadPreviousPages() {
    // If we're on the initial page (no hash exists), exit.
    if (!window.location.hash.includes('#page')) return;

    // Loop through all the viewIds, and get down to business!
    Object.keys(Drupal.pagerLoadMore).forEach((viewId) => {
      const { pagerId, contentContainer, pagerContainer } = Drupal.pagerLoadMore[viewId];

      // Generate an array of page numbers from the URL. The numbers' index will
      // correspond to its pagerId. We then generate another array with a list of
      // the pagerIds that have pages to fetch. We then check this array to make
      // sure it includes the current pagerId. If it does not, we abort.
      const pageHashArray = getPageHashFromUrl().split(',');
      const pagerIdsThatHaveData = pageHashArray.map((pageNumber, i) => {
        let item = false;
        if (parseInt(pageNumber) > 0) item = i;
        return item;
      });
      if (!pagerIdsThatHaveData.includes(pagerId)) return;

      const pageNumberFromUrl = getCurrentPageNumberFromURL(pagerId); // Page number we want to fetch.
      const pagePromises = [];
      const pagesData = {};

      // If the data is already loaded, abort.
      if (!contentContainer || contentContainer.classList.contains('is-loaded')) return;

      pagerContainer.querySelector('.pager-load-more').classList.add('is-loading');

      // Fetch each of the pages for the View.
      // Start with 1, because we don't ever want to fetch 0 (because thats the default which is already present).
      for (let i = 1; i <= pageNumberFromUrl; i++) {
        // Generate a value for the `page` querystring that will be used when constructing the URL to fetch.
        const newPageParam = generatePageParameter(i, pagerId);

        // Add all promises to array, so we can call Promises.all on it later.
        pagePromises.push(
          fetchData(newPageParam, viewId).then((pageData) => {
            // Create an object containing arrays of elements.
            // We cannot use an array because it may be out of order (if the later
            // pages are fetched before the earlier pages). So we use the object
            // key to denote the page where the array of elements belong.
            pagesData[i] = pageData;
          }),
        );
      }

      // When all fetches have completed, combine into one giant array, and pass to appendDataToView().
      Promise.all(pagePromises).then(() => {
        const combinedPagesArr = [];

        // Combine into sorted array.
        for (let i = 1; i <= Object.keys(pagesData).length; i++) {
          combinedPagesArr.push(...pagesData[i]);
        }
        appendDataToView(combinedPagesArr, viewId, false);

        // CSS class to indicate the data is loaded.
        contentContainer.classList.add('is-loaded');
      });
    });
  }

  /**
   * Handles button 'click' event.
   *
   * @param {Event} e - The event object.
   */
  function fetchNextPage(e) {
    const viewId = getViewId(e.currentTarget.closest(viewSelector));
    const { currentPage, pagerId, pagerContainer } = Drupal.pagerLoadMore[viewId];
    const newPageNumber = parseInt(currentPage) + 1;
    const newPageHash = generatePageHash(newPageNumber, pagerId);
    const nextPageParameter = generatePageParameter(newPageNumber, pagerId);

    pagerContainer.querySelector('.pager-load-more').classList.add('is-loading');

    // Update hash
    window.location.hash = `page${newPageHash}`; // @todo should we do this after the new page is loaded?

    fetchData(nextPageParameter, viewId).then((data) => appendDataToView(data, viewId));
  }

  function initPager(el) {
    const viewEl = el.closest(viewSelector);
    const viewType = getViewType(viewEl);
    const { contentContainerSelector } = viewType;
    const viewId = getViewId(viewEl); // This returns the index of the view in the DOM
    const viewIndex = viewId; // Hard code it because nothing else to use.
    const pagerId = viewId; // Hard code until we can get this via PHP.
    const morePagesExist = hasNextLink(el);

    Drupal.pagerLoadMore[viewId] = {
      pagerContainer: el,
      viewEl,
      contentContainerSelector,
      contentContainer: viewEl.querySelector(contentContainerSelector),
      viewIndex,
      pagerId,
      morePagesExist,
      viewId,
      currentPage: getCurrentPageNumberFromURL(pagerId),
      isTableView: viewType.type === 'table',
      newContentHtmlElementType: viewType.newContentHtmlElementType,
    };

    const buttonWrapper = document.createElement('div');
    buttonWrapper.classList.add('pager-load-more');
    buttonWrapper.innerHTML = `
      <button class="pager-load-more__button button button--primary" type="button">
        ${Drupal.t(drupalSettings.pager_load_more.button_text)}
        <span class="pager-load-more__button-icon"></span>
      </button>
      <div class="pager-load-more__error-container"></div>
    `;
    el.append(buttonWrapper);
    el.querySelector('.pager-load-more__button').addEventListener('click', fetchNextPage);
    el.querySelector('ul').classList.add('is-hidden-by-pager-load-more');
  }

  Drupal.behaviors.loadMore = {
    attach(context) {
      // Exit if browser doesn't support features.
      if (!browserSupportExists()) return;

      once('pager-load-more', drupalSettings.pager_load_more.pager_container_selector, context).forEach(initPager);
      once('pager-load-more-load-previous', 'body').forEach(loadPreviousPages);
    },
  };
})(Drupal, drupalSettings, once);
